﻿using FinalTest.DAL.Entities;

namespace FinalTest.DAL.Repositories.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
