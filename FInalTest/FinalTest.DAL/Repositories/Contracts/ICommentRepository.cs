﻿using System.Collections.Generic;
using FinalTest.DAL.Entities;

namespace FinalTest.DAL.Repositories.Contracts
{
    public interface ICommentRepository : IRepository<Comment>
    {
        Comment GetByIdWithAuthorsAndPlace(int id);
        IEnumerable<Comment> GetAllCommentWithAuthorAndPlace();
    }
}
