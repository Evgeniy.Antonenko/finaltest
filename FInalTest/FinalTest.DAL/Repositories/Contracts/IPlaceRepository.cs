﻿using System.Collections.Generic;
using FinalTest.DAL.Entities;

namespace FinalTest.DAL.Repositories.Contracts
{
    public interface IPlaceRepository : IRepository<Place>
    {
        IEnumerable<Place> GetAllPlacesWithAuthorsAndCommentsAndPictures();
        Place GetByIdWithAuthorsAndCommentAndPicture(int id);
    }
}
