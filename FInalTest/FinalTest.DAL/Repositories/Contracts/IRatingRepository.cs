﻿using FinalTest.DAL.Entities;

namespace FinalTest.DAL.Repositories.Contracts
{
    public interface IRatingRepository : IRepository<Rating>
    {
    }
}
