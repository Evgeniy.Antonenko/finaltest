﻿using System;
using System.Collections.Generic;
using System.Text;
using FinalTest.DAL.Entities;
using FinalTest.DAL.Repositories.Contracts;

namespace FinalTest.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Users;
        }
    }
}
