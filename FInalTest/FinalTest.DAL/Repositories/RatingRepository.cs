﻿using FinalTest.DAL.Entities;
using FinalTest.DAL.Repositories.Contracts;

namespace FinalTest.DAL.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Ratings;
        }
    }
}
