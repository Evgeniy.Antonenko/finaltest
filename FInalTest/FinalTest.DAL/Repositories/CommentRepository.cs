﻿using System.Collections.Generic;
using System.Linq;
using FinalTest.DAL.Entities;
using FinalTest.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FinalTest.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Comments;
        }

        public IEnumerable<Comment> GetAllCommentWithAuthorAndPlace()
        {
            return entities 
                .Include(c => c.Place)
                .Include(pl => pl.Author)
                .ToList();
        }

        public Comment GetByIdWithAuthorsAndPlace(int id)
        {
            return GetAllCommentWithAuthorAndPlace().FirstOrDefault(c => c.Id == id);
        }
    }
}
