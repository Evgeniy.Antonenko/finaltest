﻿using FinalTest.DAL.Entities;
using FinalTest.DAL.Repositories.Contracts;

namespace FinalTest.DAL.Repositories
{
    public class PictureRepository : Repository<Picture>, IPictureRepository
    {
        public PictureRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Pictures;
        }
    }
}
