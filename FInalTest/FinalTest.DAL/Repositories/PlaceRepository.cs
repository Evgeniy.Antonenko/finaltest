﻿using System.Collections.Generic;
using System.Linq;
using FinalTest.DAL.Entities;
using FinalTest.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FinalTest.DAL.Repositories
{
    public class PlaceRepository : Repository<Place>, IPlaceRepository
    {
        public PlaceRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Places;
        }

        public IEnumerable<Place> GetAllPlacesWithAuthorsAndCommentsAndPictures()
        {
            return entities
                .Include(pl => pl.Pictures)
                .Include(pl => pl.Comments)
                .Include(pl => pl.Ratings)
                .Include(pl => pl.Author)
                .ToList();
        }

        public Place GetByIdWithAuthorsAndCommentAndPicture(int id)
        {
            return GetAllPlacesWithAuthorsAndCommentsAndPictures().FirstOrDefault(c => c.Id == id);
        }
    }
}
