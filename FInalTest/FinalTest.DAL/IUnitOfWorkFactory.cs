﻿
namespace FinalTest.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
