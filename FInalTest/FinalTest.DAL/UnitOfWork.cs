﻿using System;
using FinalTest.DAL.Repositories;
using FinalTest.DAL.Repositories.Contracts;

namespace FinalTest.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IUserRepository Users { get; set; }
        public ICommentRepository Comments { get; set; }
        public IPictureRepository Pictures { get; set; }
        public IPlaceRepository Places { get; set; }
        public IRatingRepository Ratings { get; set; }


        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Users = new UserRepository(context);
            Comments = new CommentRepository(context);
            Pictures = new PictureRepository(context);
            Places = new PlaceRepository(context);
            Ratings = new RatingRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
