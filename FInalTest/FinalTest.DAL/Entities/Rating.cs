﻿using FinalTest.DAL.Entities.Contracts;

namespace FinalTest.DAL.Entities
{
    public class Rating : IEntity
    {
        public int Id { get; set; }

        public int Mark { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }

        public int PlaceId { get; set; }

        public Place Place { get; set; }
    }
}
