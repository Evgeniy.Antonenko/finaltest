﻿using FinalTest.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace FinalTest.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
