﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalTest.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace FinalTest.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }
        public ICollection<Comment> Comments { get; set; }

        public ICollection<Picture> Pictures { get; set; }

        public ICollection<Place> Places { get; set; }

        public ICollection<Rating> Ratings { get; set; }
    }
}
