﻿using System.Collections.Generic;
using FinalTest.DAL.Entities.Contracts;

namespace FinalTest.DAL.Entities
{
    public class Place : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string FilePath { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public ICollection<Picture> Pictures { get; set; }

        public ICollection<Rating> Ratings { get; set; }
    }
}
