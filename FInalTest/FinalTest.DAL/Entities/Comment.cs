﻿using System;
using FinalTest.DAL.Entities.Contracts;

namespace FinalTest.DAL.Entities
{
    public class Comment : IEntity
    {
        public int Id { get; set; }

        public DateTime DateOfPublication { get; set; }

        public string Content { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }

        public int PlaceId { get; set; }

        public Place Place { get; set; }
    }
}
