﻿using FinalTest.DAL.Entities.Contracts;

namespace FinalTest.DAL.Entities
{
    public class Picture : IEntity
    {
        public int Id { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }

        public int PlaceId { get; set; }

        public Place Place { get; set; }

        public string FilePath { get; set; }
    }
}
