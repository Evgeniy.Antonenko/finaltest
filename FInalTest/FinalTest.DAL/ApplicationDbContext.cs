﻿using FinalTest.DAL.Entities;
using FinalTest.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FinalTest.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        
        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity(_entityConfigurationsContainer.UserConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.CommentConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.PictureConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.PlaceConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.RatingConfiguration.ProvideConfigurationAction());

            //DisableOneToManyCascadeDelete(builder);
        }
    }
}
