﻿using FinalTest.DAL.Entities;
using FinalTest.DAL.EntitiesConfiguration.Contracts;

namespace FinalTest.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<User> UserConfiguration { get; }
        public IEntityConfiguration<Comment> CommentConfiguration { get; }
        public IEntityConfiguration<Picture> PictureConfiguration { get; }
        public IEntityConfiguration<Place> PlaceConfiguration { get; }
        public IEntityConfiguration<Rating> RatingConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            UserConfiguration = new UserConfiguration();
            CommentConfiguration = new CommentConfiguration();
            PictureConfiguration = new PictureConfiguration();
            PlaceConfiguration = new PlaceConfiguration();
            RatingConfiguration = new RatingConfiguration();
        }
    }
}
