﻿using FinalTest.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalTest.DAL.EntitiesConfiguration
{
    public class UserConfiguration : BaseEntityConfiguration<User>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<User> builder)
        {
            
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<User> builder)
        {
            builder
                .HasMany(u => u.Comments)
                .WithOne(c => c.Author)
                .HasForeignKey(c => c.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(u => u.Places)
                .WithOne(pl => pl.Author)
                .HasForeignKey(pl => pl.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(u => u.Pictures)
                .WithOne(p => p.Author)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(u => u.Ratings)
                .WithOne(r => r.Author)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
