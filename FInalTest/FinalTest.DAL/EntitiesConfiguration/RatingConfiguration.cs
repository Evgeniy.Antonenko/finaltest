﻿using FinalTest.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalTest.DAL.EntitiesConfiguration
{
    public class RatingConfiguration : BaseEntityConfiguration<Rating>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Rating> builder)
        {
            builder
                .HasIndex(p => p.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Rating> builder)
        {
            builder
                .HasOne(r => r.Author)
                .WithMany(u => u.Ratings)
                .HasForeignKey(r => r.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(r => r.Place)
                .WithMany(pl => pl.Ratings)
                .HasForeignKey(r => r.PlaceId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
