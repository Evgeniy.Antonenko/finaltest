﻿using FinalTest.DAL.Entities;

namespace FinalTest.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<User> UserConfiguration { get; }
        IEntityConfiguration<Comment> CommentConfiguration { get; }
        IEntityConfiguration<Picture> PictureConfiguration { get; }
        IEntityConfiguration<Place> PlaceConfiguration { get; }
        IEntityConfiguration<Rating> RatingConfiguration { get; }
    }
}
