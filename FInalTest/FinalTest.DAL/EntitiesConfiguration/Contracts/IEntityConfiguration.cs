﻿using System;
using FinalTest.DAL.Entities.Contracts;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalTest.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
