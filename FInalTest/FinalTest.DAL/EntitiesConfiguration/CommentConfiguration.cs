﻿using FinalTest.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalTest.DAL.EntitiesConfiguration
{
    public class CommentConfiguration : BaseEntityConfiguration<Comment>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Comment> builder)
        {
            builder
                .Property(c => c.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(c => c.DateOfPublication)
                .IsRequired();
            builder
                .HasIndex(c => c.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasOne(c => c.Author)
                .WithMany(u => u.Comments)
                .HasForeignKey(c => c.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(c => c.Place)
                .WithMany(p => p.Comments)
                .HasForeignKey(c => c.PlaceId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
