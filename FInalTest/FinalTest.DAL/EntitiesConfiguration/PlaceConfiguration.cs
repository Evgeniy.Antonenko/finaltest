﻿using FinalTest.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalTest.DAL.EntitiesConfiguration
{
    public class PlaceConfiguration : BaseEntityConfiguration<Place>
    {
        public class CommentConfiguration : BaseEntityConfiguration<Place>
        {
            protected override void ConfigureProperties(EntityTypeBuilder<Place> builder)
            {
                builder
                    .Property(pl => pl.Name)
                    .HasMaxLength(250)
                    .IsRequired();
                builder
                    .Property(pl => pl.Description)
                    .HasMaxLength(int.MaxValue)
                    .IsRequired();
                builder
                    .Property(pl => pl.FilePath)
                    .HasMaxLength(int.MaxValue)
                    .IsRequired();
                builder
                    .HasIndex(pl => pl.Id)
                    .IsUnique();
            }

            protected override void ConfigureForeignKeys(EntityTypeBuilder<Place> builder)
            {
                builder
                    .HasOne(pl => pl.Author)
                    .WithMany(u => u.Places)
                    .HasForeignKey(pl => pl.AuthorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

                builder
                    .HasMany(pl => pl.Comments)
                    .WithOne(c => c.Place)
                    .HasForeignKey(c => c.PlaceId)
                    .OnDelete(DeleteBehavior.Restrict);

                builder
                    .HasMany(pl => pl.Pictures)
                    .WithOne(p => p.Place)
                    .HasForeignKey(p => p.PlaceId)
                    .OnDelete(DeleteBehavior.Restrict);

                builder
                    .HasMany(pl => pl.Ratings)
                    .WithOne(r => r.Place)
                    .HasForeignKey(r => r.PlaceId)
                    .OnDelete(DeleteBehavior.Restrict);
            }
        }
    }
}
