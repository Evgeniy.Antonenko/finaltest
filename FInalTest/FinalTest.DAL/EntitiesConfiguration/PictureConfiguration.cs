﻿using System;
using FinalTest.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalTest.DAL.EntitiesConfiguration
{
    public class PictureConfiguration : BaseEntityConfiguration<Picture>
    {
        public class CommentConfiguration : BaseEntityConfiguration<Picture>
        {
            protected override void ConfigureProperties(EntityTypeBuilder<Picture> builder)
            {
                builder
                    .Property(p => p.FilePath)
                    .HasMaxLength(int.MaxValue)
                    .IsRequired();
                
                builder
                    .HasIndex(p => p.Id)
                    .IsUnique();
            }

            protected override void ConfigureForeignKeys(EntityTypeBuilder<Picture> builder)
            {
                builder
                    .HasOne(p => p.Author)
                    .WithMany(u => u.Pictures)
                    .HasForeignKey(p => p.AuthorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

                builder
                    .HasOne(p => p.Place)
                    .WithMany(c => c.Pictures)
                    .HasForeignKey(p => p.PlaceId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
            }
        }
    }
}
