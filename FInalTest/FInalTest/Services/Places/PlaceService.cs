﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using FinalTest.DAL;
using FinalTest.DAL.Entities;
using FInalTest.Models.CommentModels;
using FInalTest.Models.PictureModels;
using FInalTest.Models.PlaceModels;
using FInalTest.Models.RatingModels;
using FInalTest.Services.Places.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FInalTest.Services.Places
{
    public class PlaceService : IPlaceService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public PlaceService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public PlaceFilterModel GetPlaceFilterModel(PlaceFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Place> places = unitOfWork.Places.GetAllPlacesWithAuthorsAndCommentsAndPictures().ToList();
                IEnumerable<Rating> ratings = unitOfWork.Ratings.GetAll().ToList();

                places = places
                    .BySearchKey(model.SearchKey);
                
                List<PlaceModel> placeModels = Mapper.Map<List<PlaceModel>>(places);
                foreach (var placeModel in placeModels)
                {
                    var qty = (double)ratings.Where(a => a.PlaceId == placeModel.Id).Count();
                    var sum = ratings.Where(a => a.PlaceId == placeModel.Id).Sum(r => r.Mark);
                    sum = sum == 0 ? 1 : sum;
                    placeModel.RatingScore = Math.Round(sum / qty);
                    placeModel.RatingScore = placeModel.RatingScore == Double.PositiveInfinity ? 0.0 : placeModel.RatingScore;
                }

                int pageSize = 8;
                int count = places.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                PlacePageModel placePageModel = new PlacePageModel(count, page, pageSize);
                placeModels = placeModels.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                model.PlacePageModel = placePageModel;

                model.Places = placeModels;

                return model;
            }
        }

        public PlaceModel GetPlaceById(int placeId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                int numberRating = 5;
                var place = unitOfWork.Places.GetByIdWithAuthorsAndCommentAndPicture(placeId);
                var pictures = unitOfWork.Pictures.GetAll().Where(p => p.PlaceId == placeId).ToList();
                var comments = unitOfWork.Comments.GetAllCommentWithAuthorAndPlace().Where(c => c.PlaceId == placeId).ToList();
                var ratingPlace = unitOfWork.Ratings.GetAll().Where(r => r.PlaceId == placeId).ToList();
                double quantity = ratingPlace.Count();
                int sum = ratingPlace.Sum(r => r.Mark);
                sum = sum == 0 ? 1 : sum;
                double average = Math.Round((sum / quantity), 1);
                average = average == Double.PositiveInfinity ? 0.0 : average;

                place.Comments = comments;
                var placeModel = Mapper.Map<PlaceModel>(place);
                placeModel.MarkSelect = GetRatingSelect(numberRating);
                placeModel.RatingScore = average;
                placeModel.Pictures = pictures;
                return placeModel;
            }
        }

        public SelectList GetRatingSelect(int number, int? selectedNumber = null)
        {
            return new SelectList(Enumerable.Range(1, number).Select(x => new SelectListItem()
            {
                Text = x.ToString(),
                Value = x.ToString()
            }), "Value", "Text", selectedNumber);
        }

        public CommentCreateModel GetCommentCreateModel(int placeId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var commentCreateModel = new CommentCreateModel()
                {
                    PlaceId = placeId
                };

                return commentCreateModel;
            }
        }

        public void CommentCreate(CommentCreateModel model, User currentUser)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var comment = Mapper.Map<Comment>(model);
                comment.AuthorId = currentUser.Id;
                comment.DateOfPublication = DateTime.Now;
                unitOfWork.Comments.Create(comment);
            }
        }

        public void CreateRating(RatingCreateModel model, int userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                int mark = model.Mark;
                var rating = Mapper.Map<Rating>(model);
                rating.AuthorId = userId;

                unitOfWork.Ratings.Create(rating);
            }
        }

        public void UploadPictures(AddPictureModel model, int userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                if (model.Images != null)
                {
                    foreach (var image in model.Images)
                    {
                        var path = UploadImage(image);
                        var galleryImage = new Picture()
                        {
                            AuthorId = userId,
                            PlaceId = model.PlaceId,
                            FilePath = path
                        };
                        unitOfWork.Pictures.Create(galleryImage);
                    }
                }
            }
        }

        public void CreatePlace(PlaceCreateModel model, User currentUser)
        {
            model.Name = Regex.Replace(model.Name, @"\s+", " ").Trim();
            model.Description = Regex.Replace(model.Description, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var place = Mapper.Map<Place>(model);
                place.AuthorId = currentUser.Id;

                place.FilePath = UploadImage(model.ImageGet);

                unitOfWork.Places.Create(place);
            }
        }

        private string UploadImage(IFormFile image)
        {
            string fileName = $"{Guid.NewGuid()}_{image.FileName}";
            string path = @"wwwroot";
            string subpath = @"uploads\images";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string filePath = dirInfo.CreateSubdirectory(subpath).ToString();
            filePath = $@"wwwroot/uploads/images/{fileName}";
            using (var stream = System.IO.File.Create(filePath))
            {
                image.CopyTo(stream);
            }
            return $@"/uploads/images/{fileName}";
        }
    }
}
