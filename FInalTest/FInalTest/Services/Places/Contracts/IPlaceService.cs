﻿using FinalTest.DAL.Entities;
using FInalTest.Models.CommentModels;
using FInalTest.Models.PictureModels;
using FInalTest.Models.PlaceModels;
using FInalTest.Models.RatingModels;

namespace FInalTest.Services.Places.Contracts
{
    public interface IPlaceService
    {
        void CreateRating(RatingCreateModel model, int userId);
        CommentCreateModel GetCommentCreateModel(int placeId);
        void CommentCreate(CommentCreateModel model, User currentUser);
        void UploadPictures(AddPictureModel model, int userIds);
        PlaceModel GetPlaceById(int placeId);
        void CreatePlace(PlaceCreateModel model, User currentUser);
        PlaceFilterModel GetPlaceFilterModel(PlaceFilterModel model);
    }
}
