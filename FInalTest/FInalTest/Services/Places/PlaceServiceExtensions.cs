﻿using System.Collections.Generic;
using System.Linq;
using FinalTest.DAL.Entities;

namespace FInalTest.Services.Places
{
    public static class PlaceServiceExtensions
    {
        public static IEnumerable<Place> BySearchKey(this IEnumerable<Place> places, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                places = places.Where(pl => pl.Name.ToLower().Contains(searchKey) || pl.Description.ToLower().Contains(searchKey));

            return places;
        }
    }
}
