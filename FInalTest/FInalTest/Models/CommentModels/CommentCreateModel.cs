﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FInalTest.Models.CommentModels
{
    public class CommentCreateModel
    {
        [Required(ErrorMessage = "Укажите содержание комментария")]
        [Display(Name = "Контент Комментария")]
        public string Content { get; set; }

        [Display(Name = "Дата публикации Комментария")]
        public DateTime DateOfPublication { get; set; }

        [Display(Name = "ID автора коментария")]
        public int AuthorId { get; set; }

        [Display(Name = " ID заведения")]
        public int PlaceId { get; set; }
    }
}
