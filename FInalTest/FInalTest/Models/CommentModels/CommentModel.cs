﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FInalTest.Models.CommentModels
{
    public class CommentModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата публикации Комментария")]
        public DateTime DateOfPublication { get; set; }

        [Display(Name = "Контент Комментария")]
        public string Content { get; set; }

        [Display(Name = "ID автора коментария")]
        public int AuthorId { get; set; }

        [Display(Name = "Имя автора коментария")]
        public string Author { get; set; }

        [Display(Name = "коментируемое заведение")]
        public int PlaceId { get; set; }

        [Display(Name = "Название заведения")]
        public string Place { get; set; }
    }
}
