﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FInalTest.Models.PictureModels
{
    public class AddPictureModel
    {
        [Required]
        public int PlaceId { get; set; }

        [Required]
        public IFormFileCollection Images { get; set; }
    }
}
