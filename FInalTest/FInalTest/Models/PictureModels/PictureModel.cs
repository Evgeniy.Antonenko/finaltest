﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FInalTest.Models.PictureModels
{
    public class PictureModel
    {
        public int Id { get; set; }

        [Required]
        public int AuthorId { get; set; }

        [Required]
        public int PlaceId { get; set; }

        [Required]
        public IFormFileCollection Images { get; set; }
    }
}
