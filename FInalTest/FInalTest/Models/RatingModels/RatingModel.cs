﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FInalTest.Models.RatingModels
{
    public class RatingModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "необходимо выбрать оценку")]
        [Display(Name = "Оценка рейтинга")]
        public int Mark { get; set; }
        public SelectList MarkSelect { get; set; }

        [Display(Name = "ID автора рейтинга")]
        public int AuthorId { get; set; }

        [Display(Name = "Имя автора рейтинга")]
        public string Author { get; set; }

        [Display(Name = "ID заведения")]
        public int PlaceId { get; set; }

        [Display(Name = "Название заведения")]
        public string Place { get; set; }
    }
}
