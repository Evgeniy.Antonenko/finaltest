﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FInalTest.Models.PlaceModels
{
    public class PlaceCreateModel
    {
        [Display(Name = "Название заведения")]
        [Required(ErrorMessage = "Поле \"Название заведения\" должно быть заполненно")]
        public string Name { get; set; }

        [Display(Name = "Описание заведения")]
        [Required(ErrorMessage = "Поле \"Описание заведения\" должно быть заполненно")]
        public string Description { get; set; }

        [Display(Name = "Изображение заведения")]
        [Required(ErrorMessage = "Изображение должно быть выбрано")]
        public IFormFile ImageGet { get; set; }
    }
}
