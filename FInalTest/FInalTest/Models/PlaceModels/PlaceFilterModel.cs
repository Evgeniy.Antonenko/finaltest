﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FinalTest.DAL.Entities;
using FInalTest.Models.CommentModels;
using FInalTest.Models.PictureModels;
using FInalTest.Models.RatingModels;

namespace FInalTest.Models.PlaceModels
{
    public class PlaceFilterModel
    {
        [Display(Name = "По ключевому слову")]
        public string SearchKey { get; set; }

        public List<PlaceModel> Places { get; set; }

        //public PlaceModel PlaceModel { get; set; }

        public int? Page { get; set; }

        public PlacePageModel PlacePageModel { get; set; }

        //public PictureModel PictureModel { get; set; }

        //public Picture Picture { get; set; }

        //public List<Picture> Pictures { get; set; }

        //public CommentModel CommentModel { get; set; }

        //public Comment Comment { get; set; }

        //public List<Comment> Comments { get; set; }

        //public RatingModel RatingModel { get; set; }

        //public Rating Rating { get; set; }

        //public List<Rating> Ratings { get; set; }
    }
}
