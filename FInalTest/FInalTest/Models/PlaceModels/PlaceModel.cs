﻿using System.Collections.Generic;
using FinalTest.DAL.Entities;
using FInalTest.Models.CommentModels;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FInalTest.Models.PlaceModels
{
    public class PlaceModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public string FilePath { get; set; }

        public double RatingScore { get; set; }
        
        public double QtyRating { get; set; }

        public int SumRating { get; set; }

        public List<Picture> Pictures { get; set; }

        public CommentModel CommentModel { get; set; }
        
        public List<Comment> Comments { get; set; }
        
        public SelectList MarkSelect { get; set; }
    }
}
