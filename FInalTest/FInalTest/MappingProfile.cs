﻿using AutoMapper;
using FinalTest.DAL.Entities;
using FInalTest.Models.CommentModels;
using FInalTest.Models.PlaceModels;
using FInalTest.Models.RatingModels;

namespace FInalTest
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateModelMap<Place, PlaceModel>();
            CreateModelMap<Place, PlaceFilterModel>();
            CreateModelMap<Place, PlaceCreateModel>();
            CreateModelMap<Rating, RatingModel>();
            CreateModelMap<Rating, RatingCreateModel>();
            CreateModelMap<Comment, CommentModel>();
            CreateModelMap<Comment, CommentCreateModel>();
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }
    }
}
