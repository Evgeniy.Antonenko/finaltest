﻿using System;
using System.Threading.Tasks;
using FinalTest.DAL.Entities;
using FInalTest.Models.CommentModels;
using FInalTest.Models.PictureModels;
using FInalTest.Models.PlaceModels;
using FInalTest.Models.RatingModels;
using FInalTest.Services.Places.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FInalTest.Controllers
{
    public class PlaceController : Controller
    {
        private readonly IPlaceService _placeService;
        private readonly UserManager<User> _userManager;

        public PlaceController(IPlaceService placeService, UserManager<User> userManager)
        {
            if (placeService == null)
                throw new ArgumentNullException(nameof(placeService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _placeService = placeService;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index(PlaceFilterModel model, string search)
        {
            try
            {
                model.SearchKey = search;
                var places = _placeService.GetPlaceFilterModel(model);
                return View(places);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult DetailsPlace(int placeId)
        {
            var placeModel = _placeService.GetPlaceById(placeId);

            return View(placeModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> UploadPictures(AddPictureModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _placeService.UploadPictures(model, currentUser.Id);
                return RedirectToAction("DetailsPlace", "Place", new { placeId = model.PlaceId });
            }
            catch (NullReferenceException e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult AddComment(int? placeId)
        {
            if (!placeId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(placeId));

            var model = _placeService.GetCommentCreateModel(placeId.Value);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddComment(CommentCreateModel model)
        {
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);
                _placeService.CommentCreate(model, currentUser);

                return RedirectToAction("DetailsPlace", "Place", new { placeId = model.PlaceId }); ;
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> GetRating(RatingCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _placeService.CreateRating(model, currentUser.Id);
                return RedirectToAction("DetailsPlace", "Place", new { placeId = model.PlaceId });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        [Authorize]
        public IActionResult CreatePlace()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreatePlace(PlaceCreateModel model)
        {
            if (ModelState.IsValid)
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _placeService.CreatePlace(model, currentUser);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }
    }
}
